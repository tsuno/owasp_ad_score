@extends('layouts.app')

@section('content')
<div class="container">
  <h2 class="text-center">Ranking</h2>
  <table class="table table-bordered">
    <tr>
      <th>Rank</th>
      <th>Team</th>
      <th>Score</th>
    </tr>
    <tbody id="ranking">
    </tbody>
  </table>
</div>
<div class="container">
  <canvas id="chart"></canvas>
<script>
  rankingChart();
  setInterval("rankingChart()",60000);
  ranking();
  setInterval("ranking()",60000);
</script>
</div>
@endsection
