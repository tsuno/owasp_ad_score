<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Slalog;

class SLALogsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $logs = Slalog::orderBy('id', 'desc')->paginate(50);
        return view('slalogs/index', ['logs' => $logs]);
    }
}
