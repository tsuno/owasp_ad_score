<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use Validator;
use App\Sla;
use App\Team;
use App\Slalog;
use App\Rank;

class ApiController extends Controller
{
    public function histories()
    {
        $teams = Team::all();

        $ranks = [];

        foreach($teams as $team)
        {
            $ranks[$team->id]['team'] = $team;
            $ranks[$team->id]['ranks'] = Rank::orderBy('created_at', 'ASC')
            ->where('team_id', $team->id)
            ->get(['score', 'count']);
            for($i = 0; $i < count($ranks[$team->id]['ranks']); $i++)
            {
                $ranks[$team->id]['ranks'][$i]['datetime'] = $ranks[$team->id]['ranks'][$i]->getRealTime();
            }
        }
        return response()->json($ranks);
    }

    public function ranking()
    {
        $ranks = Rank::getLastRanks();

        return response()->json($ranks);
    }

    public function check(Request $request)
    {
        $res = [];
        $res['result'] = false;
        if ($request->isJson())
        {
            $validator = Validator::make($request->all(), [
                'token' => 'required',
                'team_id' => 'required|integer',
                'sla_id' => 'required|integer',
                'is_success' => 'required|integer'
            ]);

            if ($validator->fails())
            {
                var_dump($request->all());
                return Response::json($res);
            }

            $json = $request->all();

            $team = Team::find((int)$json['team_id']);
            if(!is_null($team))
            {
                if($json['token'] == $team->token)
                {
                    $sla = Sla::find((int)$json['sla_id']);
                    if(!is_null($sla))
                    {
                        $log = new Slalog;
                        $log->team_id = $team->id;
                        $log->sla_id = $sla->id;
                        if($json['is_success'] == 1)
                        {
                            $log->is_success = 1;
                        }
                        else
                        {
                            $log->is_success = 0;
                        }
                        $log->save();

                        $res['result'] = true;
                    }
                }
            }
        }
        return Response::json($res);
    }
}
